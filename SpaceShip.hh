#include "Controller.hh"

#define MAX_SPEED 5
#define MIN_SPEED 1

#define MOON_X 0
#define MOON_Y 250

class SpaceShip {

    public :
        void go();
    
        int getX();
        int getY();
        void setX(int xx);
        void setY(int yy);
        int incX(int amount);
        int incY(int amount);
        int getSpeed();
        void setSpeed(int speed);

        int left();
        int right();
        int accelerate();
        int decelerate();

        void update();

        void warn(const char * msg);
        void showStatus();

    private :
        int x = 0;
        int y = 0;
        int speed = 0;

};

