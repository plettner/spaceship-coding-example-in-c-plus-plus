#include "Controller.hh"

using namespace std;

static Keyboard *pKeyboard = new Keyboard;

enum Command Controller::getCommand() {

    int ch = pKeyboard->getChar();

    switch (ch) {
    case 'a' :
        return LEFT;
    case 'd' :
        return RIGHT;
    case 'w' :
        return ACCEL;
    case 's' :
        return DECEL;
    case 'x' :
        return ABORT;
    default :
        return UNKNOWN;
    }

}

