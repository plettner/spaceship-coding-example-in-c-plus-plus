#include <iostream>
#include "SpaceShip.hh"

using namespace std;

void SpaceShip::go() {

    Controller * pController = new Controller;

    enum Command command = UNKNOWN;

    while (command != ABORT) {

        command = pController->getCommand();

        switch (command) {
            case LEFT : 
                this->left();
                break;
            case RIGHT :
                this->right();
                break;
            case ACCEL :
                this->accelerate();
                break;
            case DECEL :
                this->decelerate();
                break;
            default :
                // Skip over invalid commands
                continue;
                break;
        }

        this->update();
        this->showStatus();

    }

}

int SpaceShip::getX() { return this->x; }
int SpaceShip::getY() { return this->y; }

void SpaceShip::setX(int xx) { this->x = xx; }
void SpaceShip::setY(int yy) { this->y = yy; }

int SpaceShip::incX(int amount) {
    this->setX(this->getX() + amount);
    return this->getX();
}

int SpaceShip::incY(int amount) { 
    this->setY(this->getY() + amount);
    return this->getY();
}

int SpaceShip::getSpeed() { return this->speed; }
void SpaceShip::setSpeed(int speed) { this->speed = speed; }

int SpaceShip::left() {
    this->setX(this->getX() - 1);
    return this->getX();
}

int SpaceShip::right() {
    this->setX(this->getX() + 1);
    return this->getX();
}

int SpaceShip::accelerate() {
    const int amount = 1;
    if (this->getSpeed() + amount > MAX_SPEED) {
        this->warn("Maximum speed reached");
    } else {
        this->setSpeed(this->getSpeed() + amount); 
    }
    return this->getSpeed();
}

int SpaceShip::decelerate() {
    const int amount = 1;
    if (this->getSpeed() - amount < MIN_SPEED) {
        this->warn("Minimum speed reached");
    } else {
        this->setSpeed(this->getSpeed() - amount); 
    }
    return this->getSpeed();
}

void SpaceShip::warn(const char * msg) {
    cerr << msg << endl;
}

void SpaceShip::showStatus() {
    int x = this->getX();
    int y = this->getY();
    cout << '(' << x << ',' << y << ')';
    if (x == MOON_X && y == MOON_Y) {
        cout << " MAN ON THE MOON";
    } else if (y > MOON_Y) {
        cout << " LOST CONTACT WITH SPACESHIP";
    }
    cout << endl;
}

void SpaceShip::update() {
    this->setY(this->getY() + this->getSpeed());
}

