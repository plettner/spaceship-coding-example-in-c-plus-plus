PROGRAM=spaceship
CPP=g++
RM=rm -f

CPPFLAGS=-g
LDFLAGS=-g

OBJS= SpaceShip.o Keyboard.o Controller.o Main.o


all : $(PROGRAM)

$(PROGRAM) : $(OBJS)
	$(CPP) $(LDFLAGS) -o spaceship $(OBJS)

SpaceShip.o : SpaceShip.cc SpaceShip.hh Controller.hh Keyboard.hh Command.hh
	$(CPP) $(CPPFLAGS) -c $< -o SpaceShip.o

Keyboard.o : Keyboard.cc Keyboard.hh
	$(CPP) $(CPPFLAGS) -c $< -o Keyboard.o

Controller.o : Controller.cc Controller.hh Keyboard.hh Command.hh Command.hh
	$(CPP) $(CPPFLAGS) -c $< -o Controller.o

Main.o : Main.cc SpaceShip.hh Controller.hh Keyboard.hh Command.hh Command.hh
	$(CPP) $(CPPFLAGS) -c $< -o Main.o

clean:
	$(RM) $(OBJS) $(PROGRAM)
