#include <iostream>
#include "SpaceShip.hh"

using namespace std;

int main(int argc, char ** argv) {

    cout << "Space ship simulation starting up" << endl;

    SpaceShip spaceShip;
    spaceShip.go();

    cout << "Space ship simulation ending" << endl;

}