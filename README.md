# SpaceShip Coding Example In C-Plus-Plus

I read about some company using a SpaceShip "simulation" as a coding example.  I decided to code it in C++ to refresh my memory about C++.

## How to Build and Run

This program uses a simple Makefile.  To build the program, simply type

```
make
```

If you make changes to the code, be sure to do a clean before building:

```
make clean
make
```

To run the program, run the executable:

```
./spaceship
```

## Requirements

The program is relatively simple.  You have a spaceship.

The ship's location is (x,y) and begins the simulation at (0,0), and 
it has a speed which also begins at 0.  The speed does not change 
unless you accelerate or decelerate.

The spaceship has four different commands:  Accelerate , decelerate, left, \and right.  Here's what happens when you issue these commands:

    accelerate:  Increase the speed by 1.  The maximum speed is 5.
    decelerate:  Decrease the speed by 1.  The minimum speed is 1.
    left: Move the ship left by 1.
    right:  Move the ship right by 1.

The ship is controlled with the typical asdw commands.  

    `w` accelerates
    `s` decelerates
    `a` moves the ship left by one
    `d` moves the ship right by one

After every valid command (asdw) the ship should output the updated
location.  In additon to the location, it should output two additional
messages based on the ship's status:

   * If the ship has reached the moon at coordinates (0,250), then 
   add " LANDED ON THE MOON" to the status message.

   * If the ship has flown past the moon (y > 250) then add 
   "LOST CONTACT WITH SPACESHIP" to the message.

For example:

```
(0,235)
(0,240)
(0,245)
(0,250) LANDED ON THE MOON
```

Or:

```
(1,240)
(2,245)
(3,250)
(3,255) LOST CONTACT WITH SPACESHIP
```
